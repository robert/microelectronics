```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
> Returns
```powershell
info: downloading installer

Welcome to Rust!

This will download and install the official compiler for the Rust
programming language, and its package manager, Cargo.

Rustup metadata and toolchains will be installed into the Rustup
home directory, located at:

  /home/pi/.rustup

This can be modified with the RUSTUP_HOME environment variable.

The Cargo home directory is located at:

  /home/pi/.cargo

This can be modified with the CARGO_HOME environment variable.

The cargo, rustc, rustup and other commands will be added to
Cargo's bin directory, located at:

  /home/pi/.cargo/bin

This path will then be added to your PATH environment variable by
modifying the profile files located at:

  /home/pi/.profile
  /home/pi/.bashrc

You can uninstall at any time with rustup self uninstall and
these changes will be reverted.

Current installation options:


   default host triple: aarch64-unknown-linux-gnu
     default toolchain: stable (default)
               profile: default
  modify PATH variable: yes

1) Proceed with standard installation (default - just press enter)
2) Customize installation
3) Cancel installation
>

info: profile set to 'default'
info: default host triple is aarch64-unknown-linux-gnu
info: syncing channel updates for 'stable-aarch64-unknown-linux-gnu'
info: latest update on 2024-03-28, rust version 1.77.1 (7cf61ebde 2024-03-27)
info: downloading component 'cargo'
  8.0 MiB /   8.0 MiB (100 %)   2.5 MiB/s in  2s ETA:  0s
info: downloading component 'clippy'
info: downloading component 'rust-docs'
 14.9 MiB /  14.9 MiB (100 %)   4.0 MiB/s in  4s ETA:  0s
info: downloading component 'rust-std'
 32.1 MiB /  32.1 MiB (100 %)   4.5 MiB/s in  8s ETA:  0s
info: downloading component 'rustc'
 74.7 MiB /  74.7 MiB (100 %)   4.7 MiB/s in 18s ETA:  0s
info: downloading component 'rustfmt'
info: installing component 'cargo'
  8.0 MiB /   8.0 MiB (100 %)   4.5 MiB/s in  1s ETA:  0s
info: installing component 'clippy'
info: installing component 'rust-docs'
 14.9 MiB /  14.9 MiB (100 %) 846.4 KiB/s in 35s ETA:  0s    
info: installing component 'rust-std'
 32.1 MiB /  32.1 MiB (100 %)   4.3 MiB/s in 13s ETA:  0s
info: installing component 'rustc'
 74.7 MiB /  74.7 MiB (100 %)   3.2 MiB/s in 30s ETA:  0s
info: installing component 'rustfmt'
info: default toolchain set to 'stable-aarch64-unknown-linux-gnu'

  stable-aarch64-unknown-linux-gnu installed - rustc 1.77.1 (7cf61ebde 2024-03-27)


Rust is installed now. Great!

To get started you may need to restart your current shell.
This would reload your PATH environment variable to include
Cargo's bin directory ($HOME/.cargo/bin).

To configure your current shell, you need to source
the corresponding env file under $HOME/.cargo.

This is usually done by running one of the following (note the leading DOT):
. "$HOME/.cargo/env"            # For sh/bash/zsh/ash/dash/pdksh
source "$HOME/.cargo/env.fish"  # For fish
```
