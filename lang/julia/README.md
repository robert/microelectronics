# Languages

- [ ] Install Julia on &#x1F427; Linux

```
curl -fsSL https://install.julialang.org | sh
```
> Returns
```powershell
info: downloading installer
Welcome to Julia!

This will download and install the official Julia Language distribution
and its version manager Juliaup.

Juliaup will be installed into the Juliaup home directory, located at:

  /home/ubuntu/.juliaup

The julia, juliaup and other commands will be added to
Juliaup's bin directory, located at:

  /home/ubuntu/.juliaup/bin

This path will then be added to your PATH environment variable by
modifying the profile files located at:

  /home/ubuntu/.bashrc
  /home/ubuntu/.profile

Julia will look for a new version of Juliaup itself every 1440 minutes when you start julia.

You can uninstall at any time with juliaup self uninstall and these
changes will be reverted.

? Do you want to install with these default configuration choices? ›
❯ Proceed with installation
  Customize installation
  Cancel installation

```

- [ ] Pick `Proceed with installation`

```powershell
Now installing Juliaup
Installing Julia 1.10.2+0.aarch64.linux.gnu
Configured the default Julia version to be 'release'.
Julia was successfully installed on your system.

Depending on which shell you are using, run one of the following
commands to reload the PATH environment variable:

  . /home/ubuntu/.bashrc
  . /home/ubuntu/.profile

```
