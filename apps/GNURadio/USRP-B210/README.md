# GNU Radio

### Testing

```
gnuradio-config-info --version
```
> 3.8.2.0

```
gnuradio-config-info --prefix
```
> /usr

```
gnuradio-config-info --enabled-components
```
> Returns
```powershell
testing-support;python-support;doxygen;sphinx;gnuradio-runtime;gr-ctrlport;* thrift;gnuradio-companion;gr-blocks;gr-fec;gr-fft;gr-filter;gr-analog;gr-digital;gr-dtv;gr-audio;* alsa;* oss;* jack;* portaudio;gr-channels;gr-qtgui;gr-trellis;gr-uhd;gr-utils;gr_modtool;gr-video-sdl;gr-vocoder;* codec2;* freedv;* gsm;gr-wavelet;gr-zeromq
```

### Running

- [ ] Export Environment Variables 

- Using Linux <span style='font-size:100px;'>&#x1F427;</span>

```
export UHD_HOME=/usr/share/uhd ;
export UHD_IMAGES_DIR=${UHD_HOME}/images
```

- Using Apple &#x1F34E;

```
export UHD_HOME=/opt/local/share/uhd ;
export UHD_IMAGES_DIR=${UHD_HOME}/images
```

## :a: &#x1F4DF; UHD Sink

```
gnuradio-companion apps/GNURadio/grc/hw_tutorial.grc 
```
> Returns
```powershell
<<< Welcome to GNU Radio Companion 3.8.2.0 >>>

Block paths:
	/usr/share/gnuradio/grc/blocks

Loading: "/home/pi/Developer/gitlab.eurecom.fr/microelectronics/apps/GNURadio/hw_tutorial.grc"
>>> Done

Generating: '/home/pi/Developer/gitlab.eurecom.fr/microelectronics/apps/GNURadio/hw_tutorial.py'

Generating: '/home/pi/Developer/gitlab.eurecom.fr/microelectronics/apps/GNURadio/hw_tutorial.py'

Executing: /usr/bin/python3 -u /home/pi/Developer/gitlab.eurecom.fr/microelectronics/apps/GNURadio/hw_tutorial.py

Warning: failed to XInitThreads()
[INFO] [UHD] linux; GNU C++ version 10.2.1 20201207; Boost_107400; UHD_3.15.0.0-4+b1
[INFO] [B200] Detected Device: B205mini
[INFO] [B200] Operating over USB 3.
[INFO] [B200] Initialize CODEC control...
[INFO] [B200] Initialize Radio control...
[INFO] [B200] Performing register loopback test... 
[INFO] [B200] Register loopback test passed
[INFO] [B200] Setting master clock rate selection to 'automatic'.
[INFO] [B200] Asking for clock rate 16.000000 MHz... 
[INFO] [B200] Actually got clock rate 16.000000 MHz.
[INFO] [B200] Asking for clock rate 61.440000 MHz... 
[INFO] [B200] Actually got clock rate 61.440000 MHz.
qt.qpa.xcb: QXcbConnection: XCB error: 148 (Unknown), sequence: 192, resource id: 0, major code: 140 (Unknown), minor code: 20

>>> Done
```

<img src=images/gnuradio-graph_hw.png  width='50%' height='50%' > </img>

| | | |
|-|-|-|
| <img src=images/gnuradio-hw1.png width='' height='' > </img> | <img src=images/gnuradio-hw2.png width='' height='' > </img> | <img src=images/gnuradio-hw3.png width='' height='' > </img> | 


## :b: &#x1F4FB; FM Receiver

```
gnuradio-companion apps/GNURadio/grc/fm_receiver_MWK.grc 
```

<img src=images/gr_fm_receiver_MWK.png  width='75%' height='75%' > </img>

| | | | |
|-|-|-|-|
| <img src=images/fm_receiver_MWK-constellation.png width='' height='' > </img>| <img src=images/fm_receiver_MWK-time-domain.png width='' height='' > </img> | <img src=images/fm_receiver_MWK-frequency.png width='' height='' > </img>	| <img src=images/fm_receiver_MWK-waterfall.png width='' height='' > </img> |


# References

- [ ] [USRP B210](https://www.youtube.com/watch?v=j8fE4KbkQAs)
- [ ] [GNU Radio Companion + USRP B210: UHF USRP Sink Block](https://www.youtube.com/watch?v=Xl4xZHIuhS8)
- [ ] [How To Build an FM Receiver with the USRP in Less Than 10 Minutes](https://www.youtube.com/watch?v=KWeY2yqwVA0)
- [ ] [How to: Macports select python](https://stackoverflow.com/questions/8201760/how-to-macports-select-python)
- [ ] [How do I solve "error: externally-managed-environment" every time I use pip 3?](https://stackoverflow.com/questions/75608323/how-do-i-solve-error-externally-managed-environment-every-time-i-use-pip-3)
- [ ] [File:USRP FM fg.png](https://wiki.gnuradio.org/index.php?title=File:USRP_FM_fg.png)
- [ ] [Guided Tutorial Hardware Considerations](https://wiki.gnuradio.org/index.php?title=Guided_Tutorial_Hardware_Considerations)
- [ ] [Designing a FM receiver with the USRP B205mini #WorldRadioDay22.](https://www.controlpaths.com/2022/02/13/designing-a-fm-receiver-with-the-usrp-b205mini-worldradioday22/) 


##### Radio

- [ ] [Liste des frequences radio FM dans le département Alpes-Maritimes](https://www.frequence-radio.com/departement.php?dep=Alpes-Maritimes_06)
- [ ] [fréquences par ville, PROVENCE-ALPES-CÔTE-D'AZUR](https://www.radioscope.fr/lien/frequences/paca.htm#:~:text=CANNES,RIRE)

| Fréquence	| Radio	| Agglomération |
|-:|-|-|
|  88.7	| RADIO France International  	| 
|  91.5	| RADIO AZUR - Alpes Maritimes	|
|  96.8	| LATINA   | CANNES |
|  98.1	| TSF Jazz | CANNES |
| 103.8	| 	FRANCE BLEU AZUR	| NICE   |
| 106.5	| RADIO RIVIERA	| MONACO   |

- [ ] [This is a list of the radio stations in Toronto](https://localwiki.org/toronto/Radio_Stations)

| Frequency	| Radio	| Comments |
|-:|-|-|
|  91.1	| [Jazz FM 91](https://jazz.fm/)	| Jazz |
|  98.1 | [CHFI-FM](https://en.wikipedia.org/wiki/CHFI-FM) | Rogers Radio |

# :o: Installation

#### On Linux (debian) 

It comes preinstalled with UHD

#### ON MacOS

```
brew install gnuradio
```

#### &#x1F4A3; Install PyBombs and GnuRadio :x: Not tested and doesn't seem to work

```
sudo port install pybombs
```
> Returns
```yaml
--->  Computing dependencies for pybombs
The following dependencies will be installed: 
 py38-build
 py38-certifi
 py38-charset-normalizer
 py38-future
 py38-idna
 py38-importlib-metadata
 py38-installer
 py38-packaging
 py38-pyproject_hooks
 py38-requests
 py38-ruamel-yaml
 py38-setuptools
 py38-six
 py38-tomli
 py38-urllib3
 py38-wheel
 py38-zipp
 python38
...
--->  Installing pybombs @2.3.5_0+python38
--->  Activating pybombs @2.3.5_0+python38
--->  Cleaning pybombs
--->  Updating database of binaries
--->  Scanning binaries for linking errors
--->  No broken files found.
--->  No broken ports found.
--->  Some of the ports you installed have notes:
  python38 has the following notes:
    To make this the default Python or Python 3 (i.e., the version run by the 'python' or 'python3' commands), run
    one or both of:
    
        sudo port select --set python python38
        sudo port select --set python3 python38
```


