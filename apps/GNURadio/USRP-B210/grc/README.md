# FM Radio

---

[TOC]

---

> using GNU Radio, a UHD: USRP Source sample rate is set to 1.024M with center frequency to 91.1M and Gain Value at 30, going through a Low Pass Filter of Decimation 4, Gain 2 and cutoff frequency 100k sent to a WBFM Receiver set to Quadrature Rate of 250k and audio decimation to 1 passing through a rational resampler with interpolation 24 and decimaiton 250 dumped into an audio sink of sample rate 24khz

The flow you've described in GNU Radio is designed to receive and demodulate a wideband FM (WBFM) signal, most likely from a radio broadcast station. Let's break down each component and its purpose:

1. **UHD: USRP Source**:
   - **Sample Rate**: 1.024M (1.024 MHz)
   - **Center Frequency**: 91.1M (91.1 MHz)
   - **Gain Value**: 30
   
   This block captures the RF signal from the Universal Software Radio Peripheral (USRP) at the specified center frequency (91.1 MHz) with a gain of 30. The sample rate of 1.024 MHz sets how often the signal is sampled.

2. **Low Pass Filter**:
   - **Decimation**: 4
   - **Gain**: 2
   - **Cutoff Frequency**: 100k
   
   After the USRP source, the signal is passed through a low pass filter to reduce high-frequency noise and unwanted signals. The decimation by 4 reduces the sample rate to $\frac{1.024M}{4} = 256 \text{kHz}$. The filter's gain of 2 amplifies the signal, and the cutoff frequency of 100 kHz ensures that frequencies above this threshold are attenuated.

3. **WBFM Receiver**:
   - **Quadrature Rate**: 250k
   - **Audio Decimation**: 1
   
   The signal is then sent to a WBFM receiver block, which demodulates the FM signal. The quadrature rate (250 kHz) matches the output sample rate from the previous block. The audio decimation of 1 means that there is no further decimation at this stage, so the audio is processed at the quadrature rate.

4. **Rational Resampler**:
   - **Interpolation**: 24
   - **Decimation**: 250
   
   The signal is resampled to match the desired audio output rate. With an interpolation of 24 and a decimation of 250, the sample rate is changed from 250 kHz to $\frac{250k \times 24}{250} = 24 \text{kHz}$.

5. **Audio Sink**:
   - **Sample Rate**: 24k
   
   Finally, the processed audio signal is sent to an audio sink at a sample rate of 24 kHz, which is a common audio sampling rate.

### Summary of Signal Flow:
- **Start**: 1.024M samples per second RF signal at 91.1 MHz, Gain 30
- **After Low Pass Filter**: 256k samples per second
- **After WBFM Receiver**: 250k samples per second (audio demodulated)
- **After Rational Resampler**: 24k samples per second
- **Final Output**: 24 kHz audio signal to an audio sink

This flow is a standard FM broadcast receiver that converts an RF signal to an audio signal for playback.

## What needs to be changed to set the Audio Sink sample rate to 48khz?

To set the Audio Sink sample rate to 48 kHz instead of 24 kHz, you'll need to adjust the **Rational Resampler** block to change the interpolation and/or decimation factors accordingly. Here's how you can do it:

### Current Setup:
- **Rational Resampler**: Interpolation = 24, Decimation = 250
- **Resulting Sample Rate**: $250k \times \frac{24}{250} = 24 \text{kHz}$

### Desired Setup:
You want the output sample rate to be 48 kHz. To achieve this, the output of the Rational Resampler should be 48 kHz.

To calculate the required interpolation and decimation factors, you can adjust the ratio between interpolation and decimation so that the output sample rate becomes 48 kHz.

1. **Keep the same input sample rate (250 kHz)**:
   - Output Sample Rate = $250k \times \frac{\text{Interpolation}}{\text{Decimation}}$

2. **Set the Output Sample Rate to 48 kHz**:
   - $48k = 250k \times \frac{\text{Interpolation}}{\text{Decimation}}$

To solve for the interpolation and decimation ratio:

   - $\frac{\text{Interpolation}}{\text{Decimation}} = \frac{48k}{250k} = \frac{48}{250} = \frac{24}{125}$

### Updated Rational Resampler Settings:
- **Interpolation**: 24
- **Decimation**: 125

### Summary:
1. Update the Rational Resampler block:
   - **Interpolation** = 24
   - **Decimation** = 125

2. Set the Audio Sink block sample rate to 48 kHz.

By making these changes, the Audio Sink will now receive a 48 kHz sample rate signal, which matches your desired output.

## What needs to be changed to set the Audio Sink sample rate to 44.1khz?

To set the Audio Sink sample rate to 44.1 kHz, you'll need to adjust the **Rational Resampler** block to achieve this specific output sample rate. Here's how to modify the interpolation and decimation factors:

### Current Setup:
- **Input Sample Rate to the Rational Resampler**: 250 kHz
- **Desired Output Sample Rate**: 44.1 kHz

### Calculation:
You want the output sample rate to be 44.1 kHz. The relationship between the input and output sample rates is given by:

$$
\text{Output Sample Rate} = \text{Input Sample Rate} \times \frac{\text{Interpolation}}{\text{Decimation}}
$$

Substituting the known values:

$$
44.1k = 250k \times \frac{\text{Interpolation}}{\text{Decimation}}
$$

$$
\frac{\text{Interpolation}}{\text{Decimation}} = \frac{44.1k}{250k} = \frac{441}{2500}
$$

To simplify this, you can multiply both the numerator and denominator by 10:

$$
\frac{\text{Interpolation}}{\text{Decimation}} = \frac{4410}{25000}
$$

You can set the interpolation to 441 and the decimation to 2500, or you can multiply both by a factor to get integers that work well within GNU Radio's resampling framework.

### Updated Rational Resampler Settings:
- **Interpolation**: 441
- **Decimation**: 2500

### Summary:
1. Update the Rational Resampler block:
   - **Interpolation** = 441
   - **Decimation** = 2500

2. Set the Audio Sink block sample rate to 44.1 kHz.

By adjusting the resampler to these values, the output sample rate will now be 44.1 kHz, matching your desired Audio Sink rate.

## What is a Wideband FM Receiver? 

A **Wideband FM Receiver (WBFM Receiver)** is a type of radio receiver designed to demodulate wideband frequency modulation (WBFM) signals. WBFM is a form of frequency modulation where the frequency deviation (the amount by which the carrier frequency varies) is large compared to the modulating signal's frequency. This is typically used for high-fidelity audio broadcasts, such as those in FM radio.

### Key Characteristics of WBFM:
- **Wide Frequency Deviation**: In WBFM, the carrier frequency can vary significantly (typically ±75 kHz) from its nominal value. This wide frequency deviation allows for the transmission of high-quality audio signals with better noise immunity.
- **High Bandwidth**: Due to the wide frequency deviation, WBFM signals require more bandwidth compared to narrowband FM (NBFM). For example, a typical FM broadcast signal occupies a bandwidth of about 200 kHz.
- **Improved Audio Quality**: The wide bandwidth allows for the transmission of high-fidelity audio, making it ideal for music and high-quality voice transmissions, such as those found in commercial FM radio.

### Components of a WBFM Receiver:
1. **Tuner**: Selects the desired frequency band and filters out signals outside the band.
2. **Demodulator**: Converts the frequency variations of the modulated signal back into the original audio or data signal. In a WBFM receiver, this block specifically demodulates wideband FM signals.
3. **Amplifier**: Boosts the demodulated signal to a level suitable for further processing or output to speakers.
4. **Filter**: Ensures that only the desired frequencies are passed to the demodulator, improving the signal-to-noise ratio.
5. **Output**: The demodulated audio signal is sent to an audio sink (like speakers or a recording device).

### Common Applications:
- **FM Radio Broadcasting**: The most common application of WBFM receivers is in commercial FM radio broadcasting, where they receive signals in the 88-108 MHz frequency range.
- **Two-Way Radio Communications**: WBFM is also used in some professional two-way radio systems for clear voice communications.
- **Satellite Communications**: Some satellite systems use wideband FM for transmitting high-quality audio signals.

### Why Wideband FM?
Wideband FM is preferred in scenarios where audio quality and resistance to noise and interference are important. The wide frequency deviation provides greater immunity to noise, making it ideal for broadcasting environments where signal quality is crucial. 

### In GNU Radio:
In GNU Radio, the WBFM Receiver block is used to demodulate signals that have been frequency modulated with wideband FM. It takes in a quadrature signal (complex signal with I and Q components) and outputs the demodulated audio signal, ready for further processing or direct playback.