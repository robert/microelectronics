# Nuand BladeRF-xa4

Here’s a breakdown of the components from the image and the corresponding Python code:

1. **RTI-SDR Source Block**: Sets up the SDR source with specific parameters such as sample rate, gain, and frequency.
2. **Frequency Xlating FIR Filter**: Performs frequency translation with decimation.
3. **Rational Resampler**: Changes the sample rate by interpolating and decimating.
4. **Low Pass Filter**: Filters the signal with a cutoff frequency.
5. **Complex to Mag**: Converts a complex signal to magnitude.
6. **WBFM Receiver**: Demodulates the Wideband FM signal.
7. **Audio Sink**: Outputs audio to the speakers.
8. **QT GUI Elements**: Add GUI sliders and range controls for visualization.

Now, I'll draft a Python script based on the components.

I attempted to simulate generating the script, but the environment does not have GNU Radio installed. However, the script above is a Python template for GNU Radio Companion (GRC) based on the blocks in the image.

You can copy and paste it into a Python file and run it in an environment where GNU Radio is installed. Let me know if you need help setting up your GNU Radio environment or further assistance with modifications.


# References

- [FM Radio with bladeRF SDR](https://www.youtube.com/watch?v=_9kyhNAAmfs)