

## Install

- Install `Miniforge3` convenient script

```
curl -fsSL -o Miniforge3-MacOSX-arm64.sh https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-MacOSX-arm64.sh
```

- Check if Script Installed

```
ls -l
```
> Returns
```powershell
total 119384
-rw-r--r--  1 user  staff  60750143 23 Nov 18:47 Miniforge3-MacOSX-arm64.sh
```

- Run convenient script

```
bash Miniforge3-MacOSX-arm64.sh
```
> Returns
```powershell

Welcome to Miniforge3 24.9.2-0

In order to continue the installation process, please review the license
agreement.
Please, press ENTER to continue
>>> 
Miniforge installer code uses BSD-3-Clause license as stated below.

Binary packages that come with it have their own licensing terms
and by installing miniforge you agree to the licensing terms of individual
packages as well. They include different OSI-approved licenses including
the GNU General Public License and can be found in pkgs/<pkg-name>/info/licenses
folders.

Miniforge installer comes with a bootstrapping executable that is used
when installing miniforge and is deleted after miniforge is installed.
The bootstrapping executable uses micromamba, cli11, cpp-filesystem,
curl, c-ares, krb5, libarchive, libev, lz4, nghttp2, openssl, libsolv,
nlohmann-json, reproc and zstd which are licensed under BSD-3-Clause,
MIT and OpenSSL licenses. Licenses and copyright notices of these
projects can be found at the following URL.
https://github.com/conda-forge/micromamba-feedstock/tree/master/recipe.

=============================================================================

Copyright (c) 2019-2022, conda-forge
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Do you accept the license terms? [yes|no]
>>> yes

Miniforge3 will now be installed into this location:
/Users/user/miniforge3

  - Press ENTER to confirm the location
  - Press CTRL-C to abort the installation
  - Or specify a different location below

[/Users/user/miniforge3] >>> 
PREFIX=/Users/user/miniforge3

Transaction

  Prefix: /Users/user/miniforge3/envs/_virtual_specs_checks

  All requested packages already installed

Dry run. Not executing the transaction.
Unpacking payload ...
Extracting bzip2-1.0.8-h99b78c6_7.conda
Extracting c-ares-1.34.3-h5505292_0.conda
Extracting ca-certificates-2024.8.30-hf0a4a13_0.conda
Extracting icu-75.1-hfee45f7_0.conda
Extracting libcxx-19.1.3-ha82da77_0.conda
Extracting libev-4.33-h93a5062_2.conda
Extracting libexpat-2.6.4-h286801f_0.conda
Extracting libffi-3.4.2-h3422bc3_5.tar.bz2
Extracting libiconv-1.17-h0d3ecfb_2.conda
Extracting libzlib-1.3.1-h8359307_2.conda
Extracting lzo-2.10-h93a5062_1001.conda
Extracting ncurses-6.5-h7bae524_1.conda
Extracting pybind11-abi-4-hd8ed1ab_3.tar.bz2
Extracting python_abi-3.12-5_cp312.conda
Extracting reproc-14.2.4.post0-h93a5062_1.conda
Extracting tzdata-2024b-hc8b5060_0.conda
Extracting xz-5.2.6-h57fd34a_0.tar.bz2
Extracting fmt-10.2.1-h2ffa867_0.conda
Extracting libedit-3.1.20191231-hc8eb9b7_2.tar.bz2
Extracting libsolv-0.7.30-h6c9b7f8_0.conda
Extracting libsqlite-3.47.0-hbaaea75_1.conda
Extracting libxml2-2.13.4-h8424949_2.conda
Extracting lz4-c-1.9.4-hb7217d7_0.conda
Extracting openssl-3.3.2-h8359307_0.conda
Extracting readline-8.2-h92ec313_1.conda
Extracting reproc-cpp-14.2.4.post0-h965bd2d_1.conda
Extracting tk-8.6.13-h5083fa2_1.conda
Extracting yaml-cpp-0.8.0-h13dd4ca_0.conda
Extracting zstd-1.5.6-hb46c0d2_0.conda
Extracting krb5-1.21.3-h237132a_0.conda
Extracting libarchive-3.7.4-h83d404f_0.conda
Extracting libnghttp2-1.64.0-h6d7220d_0.conda
Extracting libssh2-1.11.0-h7a5bd25_0.conda
Extracting python-3.12.7-h739c21a_0_cpython.conda
Extracting libcurl-8.10.1-h13a7ad3_0.conda
Extracting menuinst-2.2.0-py312h81bd7bf_0.conda
Extracting archspec-0.2.3-pyhd8ed1ab_0.conda
Extracting boltons-24.0.0-pyhd8ed1ab_0.conda
Extracting brotli-python-1.1.0-py312hde4cb15_2.conda
Extracting certifi-2024.8.30-pyhd8ed1ab_0.conda
Extracting charset-normalizer-3.4.0-pyhd8ed1ab_0.conda
Extracting colorama-0.4.6-pyhd8ed1ab_0.tar.bz2
Extracting distro-1.9.0-pyhd8ed1ab_0.conda
Extracting frozendict-2.4.6-py312h0bf5046_0.conda
Extracting hpack-4.0.0-pyh9f0ad1d_0.tar.bz2
Extracting hyperframe-6.0.1-pyhd8ed1ab_0.tar.bz2
Extracting idna-3.10-pyhd8ed1ab_0.conda
Extracting jsonpointer-3.0.0-py312h81bd7bf_1.conda
Extracting libmamba-1.5.9-hbfbf5c4_0.conda
Extracting packaging-24.1-pyhd8ed1ab_0.conda
Extracting platformdirs-4.3.6-pyhd8ed1ab_0.conda
Extracting pluggy-1.5.0-pyhd8ed1ab_0.conda
Extracting pycosat-0.6.6-py312h02f2b3b_0.conda
Extracting pycparser-2.22-pyhd8ed1ab_0.conda
Extracting pysocks-1.7.1-pyha2e5f31_6.tar.bz2
Extracting ruamel.yaml.clib-0.2.8-py312h0bf5046_1.conda
Extracting setuptools-75.3.0-pyhd8ed1ab_0.conda
Extracting truststore-0.10.0-pyhd8ed1ab_0.conda
Extracting wheel-0.45.0-pyhd8ed1ab_0.conda
Extracting cffi-1.17.1-py312h0fad829_0.conda
Extracting h2-4.1.0-pyhd8ed1ab_0.tar.bz2
Extracting jsonpatch-1.33-pyhd8ed1ab_0.conda
Extracting libmambapy-1.5.9-py312h1ed1908_0.conda
Extracting pip-24.3.1-pyh8b19718_0.conda
Extracting ruamel.yaml-0.18.6-py312h0bf5046_1.conda
Extracting tqdm-4.67.0-pyhd8ed1ab_0.conda
Extracting zstandard-0.23.0-py312h15fbf35_1.conda
Extracting conda-package-streaming-0.11.0-pyhd8ed1ab_0.conda
Extracting urllib3-2.2.3-pyhd8ed1ab_0.conda
Extracting conda-package-handling-2.4.0-pyh7900ff3_0.conda
Extracting requests-2.32.3-pyhd8ed1ab_0.conda
Extracting conda-libmamba-solver-24.9.0-pyhd8ed1ab_0.conda
Extracting conda-24.9.2-py312h81bd7bf_0.conda
Extracting mamba-1.5.9-py312h14bc7db_0.conda

Installing base environment...

Transaction

  Prefix: /Users/user/miniforge3

  Updating specs:

   - conda-forge/osx-arm64::bzip2==1.0.8=h99b78c6_7[md5=fc6948412dbbbe9a4c9ddbbcfe0a79ab]
   - conda-forge/osx-arm64::c-ares==1.34.3=h5505292_0[md5=d0155a4f41f28628c7409ea000eeb19c]
   - conda-forge/osx-arm64::ca-certificates==2024.8.30=hf0a4a13_0[md5=40dec13fd8348dbe303e57be74bd3d35]
   - conda-forge/osx-arm64::icu==75.1=hfee45f7_0[md5=5eb22c1d7b3fc4abb50d92d621583137]
   - conda-forge/osx-arm64::libcxx==19.1.3=ha82da77_0[md5=bf691071fba4734984231617783225bc]
   - conda-forge/osx-arm64::libev==4.33=h93a5062_2[md5=36d33e440c31857372a72137f78bacf5]
   - conda-forge/osx-arm64::libexpat==2.6.4=h286801f_0[md5=38d2656dd914feb0cab8c629370768bf]
   - conda-forge/osx-arm64::libffi==3.4.2=h3422bc3_5[md5=086914b672be056eb70fd4285b6783b6]
   - conda-forge/osx-arm64::libiconv==1.17=h0d3ecfb_2[md5=69bda57310071cf6d2b86caf11573d2d]
   - conda-forge/osx-arm64::libzlib==1.3.1=h8359307_2[md5=369964e85dc26bfe78f41399b366c435]
   - conda-forge/osx-arm64::lzo==2.10=h93a5062_1001[md5=915996063a7380c652f83609e970c2a7]
   - conda-forge/osx-arm64::ncurses==6.5=h7bae524_1[md5=cb2b0ea909b97b3d70cd3921d1445e1a]
   - conda-forge/noarch::pybind11-abi==4=hd8ed1ab_3[md5=878f923dd6acc8aeb47a75da6c4098be]
   - conda-forge/osx-arm64::python_abi==3.12=5_cp312[md5=b76f9b1c862128e56ac7aa8cd2333de9]
   - conda-forge/osx-arm64::reproc==14.2.4.post0=h93a5062_1[md5=ef7ae6d7bb50c8c735551d825e1ea287]
   - conda-forge/noarch::tzdata==2024b=hc8b5060_0[md5=8ac3367aafb1cc0a068483c580af8015]
   - conda-forge/osx-arm64::xz==5.2.6=h57fd34a_0[md5=39c6b54e94014701dd157f4f576ed211]
   - conda-forge/osx-arm64::fmt==10.2.1=h2ffa867_0[md5=8cccde6755bdd787f9840f38a34b4e7d]
   - conda-forge/osx-arm64::libedit==3.1.20191231=hc8eb9b7_2[md5=30e4362988a2623e9eb34337b83e01f9]
   - conda-forge/osx-arm64::libsolv==0.7.30=h6c9b7f8_0[md5=a5795a7ca73c9c99f112abce7864b500]
   - conda-forge/osx-arm64::libsqlite==3.47.0=hbaaea75_1[md5=07a14fbe439eef078cc479deca321161]
   - conda-forge/osx-arm64::libxml2==2.13.4=h8424949_2[md5=3f0764c38bc02720231d49d6035531f2]
   - conda-forge/osx-arm64::lz4-c==1.9.4=hb7217d7_0[md5=45505bec548634f7d05e02fb25262cb9]
   - conda-forge/osx-arm64::openssl==3.3.2=h8359307_0[md5=1773ebccdc13ec603356e8ff1db9e958]
   - conda-forge/osx-arm64::readline==8.2=h92ec313_1[md5=8cbb776a2f641b943d413b3e19df71f4]
   - conda-forge/osx-arm64::reproc-cpp==14.2.4.post0=h965bd2d_1[md5=f81d00496e13ee828f84b3ef17e41346]
   - conda-forge/osx-arm64::tk==8.6.13=h5083fa2_1[md5=b50a57ba89c32b62428b71a875291c9b]
   - conda-forge/osx-arm64::yaml-cpp==0.8.0=h13dd4ca_0[md5=e783a232972a5c7dca549111e63a78b2]
   - conda-forge/osx-arm64::zstd==1.5.6=hb46c0d2_0[md5=d96942c06c3e84bfcc5efb038724a7fd]
   - conda-forge/osx-arm64::krb5==1.21.3=h237132a_0[md5=c6dc8a0fdec13a0565936655c33069a1]
   - conda-forge/osx-arm64::libarchive==3.7.4=h83d404f_0[md5=8b604ee634caafd92f2ff2fab6a1f75a]
   - conda-forge/osx-arm64::libnghttp2==1.64.0=h6d7220d_0[md5=3408c02539cee5f1141f9f11450b6a51]
   - conda-forge/osx-arm64::libssh2==1.11.0=h7a5bd25_0[md5=029f7dc931a3b626b94823bc77830b01]
   - conda-forge/osx-arm64::python==3.12.7=h739c21a_0_cpython[md5=e0d82e57ebb456077565e6d82cd4a323]
   - conda-forge/osx-arm64::libcurl==8.10.1=h13a7ad3_0[md5=d84030d0863ffe7dea00b9a807fee961]
   - conda-forge/osx-arm64::menuinst==2.2.0=py312h81bd7bf_0[md5=4ecad32f75f4ad25268e38778cac2b7f]
   - conda-forge/noarch::archspec==0.2.3=pyhd8ed1ab_0[md5=192278292e20704f663b9c766909d67b]
   - conda-forge/noarch::boltons==24.0.0=pyhd8ed1ab_0[md5=61de176bd62041f9cd5bd4fcd09eb0ff]
   - conda-forge/osx-arm64::brotli-python==1.1.0=py312hde4cb15_2[md5=a83c2ef76ccb11bc2349f4f17696b15d]
   - conda-forge/noarch::certifi==2024.8.30=pyhd8ed1ab_0[md5=12f7d00853807b0531775e9be891cb11]
   - conda-forge/noarch::charset-normalizer==3.4.0=pyhd8ed1ab_0[md5=a374efa97290b8799046df7c5ca17164]
   - conda-forge/noarch::colorama==0.4.6=pyhd8ed1ab_0[md5=3faab06a954c2a04039983f2c4a50d99]
   - conda-forge/noarch::distro==1.9.0=pyhd8ed1ab_0[md5=bbdb409974cd6cb30071b1d978302726]
   - conda-forge/osx-arm64::frozendict==2.4.6=py312h0bf5046_0[md5=22df6d6ec0345fc46182ce47e7ee8e24]
   - conda-forge/noarch::hpack==4.0.0=pyh9f0ad1d_0[md5=914d6646c4dbb1fd3ff539830a12fd71]
   - conda-forge/noarch::hyperframe==6.0.1=pyhd8ed1ab_0[md5=9f765cbfab6870c8435b9eefecd7a1f4]
   - conda-forge/noarch::idna==3.10=pyhd8ed1ab_0[md5=7ba2ede0e7c795ff95088daf0dc59753]
   - conda-forge/osx-arm64::jsonpointer==3.0.0=py312h81bd7bf_1[md5=80f403c03290e1662be03e026fb5f8ab]
   - conda-forge/osx-arm64::libmamba==1.5.9=hbfbf5c4_0[md5=ab849f9089963feb44836755fa4edf96]
   - conda-forge/noarch::packaging==24.1=pyhd8ed1ab_0[md5=cbe1bb1f21567018ce595d9c2be0f0db]
   - conda-forge/noarch::platformdirs==4.3.6=pyhd8ed1ab_0[md5=fd8f2b18b65bbf62e8f653100690c8d2]
   - conda-forge/noarch::pluggy==1.5.0=pyhd8ed1ab_0[md5=d3483c8fc2dc2cc3f5cf43e26d60cabf]
   - conda-forge/osx-arm64::pycosat==0.6.6=py312h02f2b3b_0[md5=4d07092345b6e66e580ce3cd9141c6da]
   - conda-forge/noarch::pycparser==2.22=pyhd8ed1ab_0[md5=844d9eb3b43095b031874477f7d70088]
   - conda-forge/noarch::pysocks==1.7.1=pyha2e5f31_6[md5=2a7de29fb590ca14b5243c4c812c8025]
   - conda-forge/osx-arm64::ruamel.yaml.clib==0.2.8=py312h0bf5046_1[md5=2ed5f254c9ea57b6d0fd4e12baa4b87f]
   - conda-forge/noarch::setuptools==75.3.0=pyhd8ed1ab_0[md5=2ce9825396daf72baabaade36cee16da]
   - conda-forge/noarch::truststore==0.10.0=pyhd8ed1ab_0[md5=ad1c20cd193e3044bcf17798c33b9d67]
   - conda-forge/noarch::wheel==0.45.0=pyhd8ed1ab_0[md5=f9751d7c71df27b2d29f5cab3378982e]
   - conda-forge/osx-arm64::cffi==1.17.1=py312h0fad829_0[md5=19a5456f72f505881ba493979777b24e]
   - conda-forge/noarch::h2==4.1.0=pyhd8ed1ab_0[md5=b748fbf7060927a6e82df7cb5ee8f097]
   - conda-forge/noarch::jsonpatch==1.33=pyhd8ed1ab_0[md5=bfdb7c5c6ad1077c82a69a8642c87aff]
   - conda-forge/osx-arm64::libmambapy==1.5.9=py312h1ed1908_0[md5=d1607ea8bfe7e164ce3f3027615a8b7a]
   - conda-forge/noarch::pip==24.3.1=pyh8b19718_0[md5=5dd546fe99b44fda83963d15f84263b7]
   - conda-forge/osx-arm64::ruamel.yaml==0.18.6=py312h0bf5046_1[md5=c67fe5e10c151ef58bfc255b30f35f29]
   - conda-forge/noarch::tqdm==4.67.0=pyhd8ed1ab_0[md5=196a9e6ab4e036ceafa516ea036619b0]
   - conda-forge/osx-arm64::zstandard==0.23.0=py312h15fbf35_1[md5=a4cde595509a7ad9c13b1a3809bcfe51]
   - conda-forge/noarch::conda-package-streaming==0.11.0=pyhd8ed1ab_0[md5=bc9533d8616a97551ed144789bf9c1cd]
   - conda-forge/noarch::urllib3==2.2.3=pyhd8ed1ab_0[md5=6b55867f385dd762ed99ea687af32a69]
   - conda-forge/noarch::conda-package-handling==2.4.0=pyh7900ff3_0[md5=686fb26b6fd490b533ec580da90b2af8]
   - conda-forge/noarch::requests==2.32.3=pyhd8ed1ab_0[md5=5ede4753180c7a550a443c430dc8ab52]
   - conda-forge/noarch::conda-libmamba-solver==24.9.0=pyhd8ed1ab_0[md5=45378d089c5f72c9c0d63d58414c645d]
   - conda-forge/osx-arm64::conda==24.9.2=py312h81bd7bf_0[md5=33246b020f664b7404cadc0e266f5b16]
   - conda-forge/osx-arm64::mamba==1.5.9=py312h14bc7db_0[md5=c0eab110ba79ef3fb3633674df2b3e36]


  Package                         Version  Build               Channel         Size
─────────────────────────────────────────────────────────────────────────────────────
  Install:
─────────────────────────────────────────────────────────────────────────────────────

  + bzip2                           1.0.8  h99b78c6_7          conda-forge         
  + c-ares                         1.34.3  h5505292_0          conda-forge         
  + ca-certificates             2024.8.30  hf0a4a13_0          conda-forge         
  + icu                              75.1  hfee45f7_0          conda-forge         
  + libcxx                         19.1.3  ha82da77_0          conda-forge         
  + libev                            4.33  h93a5062_2          conda-forge         
  + libexpat                        2.6.4  h286801f_0          conda-forge         
  + libffi                          3.4.2  h3422bc3_5          conda-forge         
  + libiconv                         1.17  h0d3ecfb_2          conda-forge         
  + libzlib                         1.3.1  h8359307_2          conda-forge         
  + lzo                              2.10  h93a5062_1001       conda-forge         
  + ncurses                           6.5  h7bae524_1          conda-forge         
  + pybind11-abi                        4  hd8ed1ab_3          conda-forge         
  + python_abi                       3.12  5_cp312             conda-forge         
  + reproc                   14.2.4.post0  h93a5062_1          conda-forge         
  + tzdata                          2024b  hc8b5060_0          conda-forge         
  + xz                              5.2.6  h57fd34a_0          conda-forge         
  + fmt                            10.2.1  h2ffa867_0          conda-forge         
  + libedit                  3.1.20191231  hc8eb9b7_2          conda-forge         
  + libsolv                        0.7.30  h6c9b7f8_0          conda-forge         
  + libsqlite                      3.47.0  hbaaea75_1          conda-forge         
  + libxml2                        2.13.4  h8424949_2          conda-forge         
  + lz4-c                           1.9.4  hb7217d7_0          conda-forge         
  + openssl                         3.3.2  h8359307_0          conda-forge         
  + readline                          8.2  h92ec313_1          conda-forge         
  + reproc-cpp               14.2.4.post0  h965bd2d_1          conda-forge         
  + tk                             8.6.13  h5083fa2_1          conda-forge         
  + yaml-cpp                        0.8.0  h13dd4ca_0          conda-forge         
  + zstd                            1.5.6  hb46c0d2_0          conda-forge         
  + krb5                           1.21.3  h237132a_0          conda-forge         
  + libarchive                      3.7.4  h83d404f_0          conda-forge         
  + libnghttp2                     1.64.0  h6d7220d_0          conda-forge         
  + libssh2                        1.11.0  h7a5bd25_0          conda-forge         
  + python                         3.12.7  h739c21a_0_cpython  conda-forge         
  + libcurl                        8.10.1  h13a7ad3_0          conda-forge         
  + menuinst                        2.2.0  py312h81bd7bf_0     conda-forge         
  + archspec                        0.2.3  pyhd8ed1ab_0        conda-forge         
  + boltons                        24.0.0  pyhd8ed1ab_0        conda-forge         
  + brotli-python                   1.1.0  py312hde4cb15_2     conda-forge         
  + certifi                     2024.8.30  pyhd8ed1ab_0        conda-forge         
  + charset-normalizer              3.4.0  pyhd8ed1ab_0        conda-forge         
  + colorama                        0.4.6  pyhd8ed1ab_0        conda-forge         
  + distro                          1.9.0  pyhd8ed1ab_0        conda-forge         
  + frozendict                      2.4.6  py312h0bf5046_0     conda-forge         
  + hpack                           4.0.0  pyh9f0ad1d_0        conda-forge         
  + hyperframe                      6.0.1  pyhd8ed1ab_0        conda-forge         
  + idna                             3.10  pyhd8ed1ab_0        conda-forge         
  + jsonpointer                     3.0.0  py312h81bd7bf_1     conda-forge         
  + libmamba                        1.5.9  hbfbf5c4_0          conda-forge         
  + packaging                        24.1  pyhd8ed1ab_0        conda-forge         
  + platformdirs                    4.3.6  pyhd8ed1ab_0        conda-forge         
  + pluggy                          1.5.0  pyhd8ed1ab_0        conda-forge         
  + pycosat                         0.6.6  py312h02f2b3b_0     conda-forge         
  + pycparser                        2.22  pyhd8ed1ab_0        conda-forge         
  + pysocks                         1.7.1  pyha2e5f31_6        conda-forge         
  + ruamel.yaml.clib                0.2.8  py312h0bf5046_1     conda-forge         
  + setuptools                     75.3.0  pyhd8ed1ab_0        conda-forge         
  + truststore                     0.10.0  pyhd8ed1ab_0        conda-forge         
  + wheel                          0.45.0  pyhd8ed1ab_0        conda-forge         
  + cffi                           1.17.1  py312h0fad829_0     conda-forge         
  + h2                              4.1.0  pyhd8ed1ab_0        conda-forge         
  + jsonpatch                        1.33  pyhd8ed1ab_0        conda-forge         
  + libmambapy                      1.5.9  py312h1ed1908_0     conda-forge         
  + pip                            24.3.1  pyh8b19718_0        conda-forge         
  + ruamel.yaml                    0.18.6  py312h0bf5046_1     conda-forge         
  + tqdm                           4.67.0  pyhd8ed1ab_0        conda-forge         
  + zstandard                      0.23.0  py312h15fbf35_1     conda-forge         
  + conda-package-streaming        0.11.0  pyhd8ed1ab_0        conda-forge         
  + urllib3                         2.2.3  pyhd8ed1ab_0        conda-forge         
  + conda-package-handling          2.4.0  pyh7900ff3_0        conda-forge         
  + requests                       2.32.3  pyhd8ed1ab_0        conda-forge         
  + conda-libmamba-solver          24.9.0  pyhd8ed1ab_0        conda-forge         
  + conda                          24.9.2  py312h81bd7bf_0     conda-forge         
  + mamba                           1.5.9  py312h14bc7db_0     conda-forge         

  Summary:

  Install: 74 packages

  Total download: 0 B

─────────────────────────────────────────────────────────────────────────────────────



Transaction starting

Transaction finished

To activate this environment, use:

    micromamba activate /Users/user/miniforge3

Or to execute a single command in this environment, use:

    micromamba run -p /Users/user/miniforge3 mycommand

installation finished.
Do you wish to update your shell profile to automatically initialize conda?
This will activate conda on startup and change the command prompt when activated.
If you'd prefer that conda's base environment not be activated on startup,
   run the following command when conda is activated:

conda config --set auto_activate_base false

You can undo this by running `conda init --reverse $SHELL`? [yes|no]
[no] >>> 

You have chosen to not have conda modify your shell scripts at all.
To activate conda's base environment in your current shell session:

eval "$(/Users/user/miniforge3/bin/conda shell.YOUR_SHELL_NAME hook)" 

To install conda's shell functions for easier access, first activate, then:

conda init
```

## Create gnuradio-env

```
conda create -n gnuradio-env gnuradio
```
> Returns
```powershell
Channels:
 - conda-forge
Platform: osx-arm64
Collecting package metadata (repodata.json): done
Solving environment: done

## Package Plan ##

  environment location: /Users/valiha/miniforge3/envs/gnuradio-env

  added / updated specs:
    - gnuradio


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    adwaita-icon-theme-47.0    |           unix_0         554 KB  conda-forge
    atk-1.0-2.38.0             |       hd03087b_2         339 KB  conda-forge
    attrs-24.2.0               |     pyh71513ae_0          55 KB  conda-forge
    brotli-1.1.0               |       hd74edd7_2          19 KB  conda-forge
    brotli-bin-1.1.0           |       hd74edd7_2          16 KB  conda-forge
    cairo-1.18.0               |       hb4a6bf7_3         878 KB  conda-forge
    click-8.1.7                |unix_pyh707e725_0          82 KB  conda-forge
    click-plugins-1.1.1        |             py_0           9 KB  conda-forge
    contourpy-1.3.1            |  py312hb23fbb9_0         240 KB  conda-forge
    cycler-0.12.1              |     pyhd8ed1ab_0          13 KB  conda-forge
    epoxy-1.5.10               |       h1c322ee_1         347 KB  conda-forge
    fftw-3.3.10                |nompi_h6637ab6_110         745 KB  conda-forge
    fmt-11.0.2                 |       h420ef59_0         175 KB  conda-forge
    font-ttf-dejavu-sans-mono-2.37|       hab24e00_0         388 KB  conda-forge
    font-ttf-inconsolata-3.000 |       h77eed37_0          94 KB  conda-forge
    font-ttf-source-code-pro-2.038|       h77eed37_0         684 KB  conda-forge
    font-ttf-ubuntu-0.83       |       h77eed37_3         1.5 MB  conda-forge
    fontconfig-2.15.0          |       h1383a14_1         229 KB  conda-forge
    fonts-conda-ecosystem-1    |                0           4 KB  conda-forge
    fonts-conda-forge-1        |                0           4 KB  conda-forge
    fonttools-4.55.0           |  py312h998013c_0         2.6 MB  conda-forge
    freetype-2.12.1            |       hadb7bae_2         582 KB  conda-forge
    fribidi-1.0.10             |       h27ca646_0          59 KB  conda-forge
    gdk-pixbuf-2.42.12         |       h7ddc832_0         498 KB  conda-forge
    gettext-0.22.5             |       h8414b35_3         472 KB  conda-forge
    gettext-tools-0.22.5       |       h8414b35_3         2.4 MB  conda-forge
    glib-2.82.2                |       hb1db9eb_0         571 KB  conda-forge
    glib-tools-2.82.2          |       h25d4a46_0          98 KB  conda-forge
    gmp-6.3.0                  |       h7bae524_2         357 KB  conda-forge
    gnuradio-3.10.11.0         |  py312h19d1795_5          60 KB  conda-forge
    gnuradio-core-3.10.11.0    |  py312h9c444a1_5         4.8 MB  conda-forge
    gnuradio-grc-3.10.11.0     |  py312h14fb442_5         726 KB  conda-forge
    gnuradio-iio-3.10.11.0     |  py312hc9ed232_5         231 KB  conda-forge
    gnuradio-pmt-3.10.11.0     |  py312h2d65830_5         248 KB  conda-forge
    gnuradio-qtgui-3.10.11.0   |  py312hd34dc92_5         901 KB  conda-forge
    gnuradio-soapy-3.10.11.0   |  py312h3cf95d6_5         226 KB  conda-forge
    gnuradio-uhd-3.10.11.0     |  py312hbfa0caa_5         476 KB  conda-forge
    gnuradio-video-sdl-3.10.11.0|  py312h14fb442_5         129 KB  conda-forge
    gnuradio-zeromq-3.10.11.0  |  py312h17788b2_5         169 KB  conda-forge
    graphite2-1.3.13           |    hebf3989_1003          78 KB  conda-forge
    gsl-2.7                    |       h6e638da_0         2.6 MB  conda-forge
    gst-plugins-base-1.24.7    |       hb49d354_0         1.9 MB  conda-forge
    gstreamer-1.24.7           |       hc3f5269_0         1.3 MB  conda-forge
    gstreamer-orc-0.4.40       |       hd74edd7_0         244 KB  conda-forge
    gtk3-3.24.43               |       h7492e44_1         8.6 MB  conda-forge
    harfbuzz-9.0.0             |       h997cde5_1         1.3 MB  conda-forge
    hicolor-icon-theme-0.17    |       hce30654_2          13 KB  conda-forge
    importlib-metadata-8.5.0   |     pyha770c72_0          28 KB  conda-forge
    importlib_resources-6.4.5  |     pyhd8ed1ab_0          32 KB  conda-forge
    jsonschema-4.23.0          |     pyhd8ed1ab_0          73 KB  conda-forge
    jsonschema-specifications-2024.10.1|     pyhd8ed1ab_0          16 KB  conda-forge
    kiwisolver-1.4.7           |  py312h6142ec9_0          60 KB  conda-forge
    lame-3.100                 |    h1a8c8d9_1003         516 KB  conda-forge
    lcms2-2.16                 |       ha0e7c42_0         207 KB  conda-forge
    lerc-4.0.0                 |       h9a09cb3_0         211 KB  conda-forge
    libad9361-iio-c-0.3        |       ha39782d_0          99 KB  conda-forge
    libasprintf-0.22.5         |       h8414b35_3          40 KB  conda-forge
    libasprintf-devel-0.22.5   |       h8414b35_3          34 KB  conda-forge
    libblas-3.9.0              |25_osxarm64_openblas          16 KB  conda-forge
    libboost-1.86.0            |       h29978a0_2         1.9 MB  conda-forge
    libbrotlicommon-1.1.0      |       hd74edd7_2          67 KB  conda-forge
    libbrotlidec-1.1.0         |       hd74edd7_2          28 KB  conda-forge
    libbrotlienc-1.1.0         |       hd74edd7_2         273 KB  conda-forge
    libcblas-3.9.0             |25_osxarm64_openblas          15 KB  conda-forge
    libclang-cpp17-17.0.6      |default_h146c034_7        11.8 MB  conda-forge
    libclang13-19.1.4          |default_h81d93ff_0         8.1 MB  conda-forge
    libcodec2-1.2.0            |       hd74edd7_2         670 KB  conda-forge
    libcxx-19.1.4              |       ha82da77_0         508 KB  conda-forge
    libdeflate-1.22            |       hd74edd7_0          53 KB  conda-forge
    libevent-2.1.12            |       h2757513_1         360 KB  conda-forge
    libflac-1.4.3              |       hb765f3a_0         307 KB  conda-forge
    libgettextpo-0.22.5        |       h8414b35_3         156 KB  conda-forge
    libgettextpo-devel-0.22.5  |       h8414b35_3          36 KB  conda-forge
    libgfortran-5.0.0          |13_2_0_hd922786_3         108 KB  conda-forge
    libgfortran5-13.2.0        |       hf226fd6_3         974 KB  conda-forge
    libgirepository-1.82.0     |       h607895c_0         283 KB  conda-forge
    libglib-2.82.2             |       h07bd6cf_0         3.5 MB  conda-forge
    libiio-c-0.26              |       h34e41b8_0         101 KB  conda-forge
    libintl-0.22.5             |       h8414b35_3          79 KB  conda-forge
    libintl-devel-0.22.5       |       h8414b35_3          38 KB  conda-forge
    libjpeg-turbo-3.0.0        |       hb547adb_1         535 KB  conda-forge
    liblapack-3.9.0            |25_osxarm64_openblas          15 KB  conda-forge
    libllvm17-17.0.6           |       h5090b49_2        23.5 MB  conda-forge
    libllvm19-19.1.4           |       hc4b4ae8_0        25.8 MB  conda-forge
    libogg-1.3.5               |       h99b78c6_0         201 KB  conda-forge
    libopenblas-0.3.28         |openmp_hf332438_1         4.0 MB  conda-forge
    libopus-1.3.1              |       h27ca646_1         247 KB  conda-forge
    libpng-1.6.44              |       hc14010f_0         257 KB  conda-forge
    libpq-16.6                 |       h0224a88_0         2.3 MB  conda-forge
    librsvg-2.58.4             |       h40956f1_0         4.5 MB  conda-forge
    libsndfile-1.2.2           |       h9739721_1         310 KB  conda-forge
    libsodium-1.0.20           |       h99b78c6_0         161 KB  conda-forge
    libthrift-0.21.0           |       h64651cc_0         317 KB  conda-forge
    libtiff-4.7.0              |       hfce79cd_1         358 KB  conda-forge
    libusb-1.0.27              |     h93a5062_100          79 KB  conda-forge
    libvorbis-1.3.7            |       h9f76cd9_0         249 KB  conda-forge
    libwebp-base-1.4.0         |       h93a5062_0         281 KB  conda-forge
    libxcb-1.17.0              |       hdb1d25a_0         316 KB  conda-forge
    libxml2-2.13.5             |       hbbdcc80_0         569 KB  conda-forge
    libxslt-1.1.39             |       h223e5b9_0         220 KB  conda-forge
    llvm-openmp-19.1.4         |       hdb05f8b_0         275 KB  conda-forge
    lxml-5.3.0                 |  py312ha59c1f6_2         1.1 MB  conda-forge
    mako-1.3.6                 |     pyhff2d567_0          65 KB  conda-forge
    markupsafe-3.0.2           |  py312ha0ccf2a_0          24 KB  conda-forge
    matplotlib-base-3.9.2      |  py312h9bd0bc6_2         7.4 MB  conda-forge
    mpg123-1.32.9              |       hf642e45_0         352 KB  conda-forge
    munkres-1.1.4              |     pyh9f0ad1d_0          12 KB  conda-forge
    mysql-common-9.0.1         |       h0887d5e_2         616 KB  conda-forge
    mysql-libs-9.0.1           |       he9bc4e1_2         1.3 MB  conda-forge
    nspr-4.36                  |       h5833ebf_0         198 KB  conda-forge
    nss-3.107                  |       hc555b47_0         1.7 MB  conda-forge
    numpy-2.1.3                |  py312h94ee1e1_0         6.1 MB  conda-forge
    openjpeg-2.5.2             |       h9f1df11_0         309 KB  conda-forge
    openssl-3.4.0              |       h39f12f2_0         2.8 MB  conda-forge
    packaging-24.2             |     pyhff2d567_1          59 KB  conda-forge
    pango-1.54.0               |       h9ee27a3_2         407 KB  conda-forge
    pcre2-10.44                |       h297a79d_2         604 KB  conda-forge
    pillow-11.0.0              |  py312haf37ca6_0        39.8 MB  conda-forge
    pixman-0.43.4              |       hebf3989_0         194 KB  conda-forge
    pkgutil-resolve-name-1.3.10|     pyhd8ed1ab_1          11 KB  conda-forge
    ply-3.11                   |     pyhd8ed1ab_2          48 KB  conda-forge
    portaudio-19.7.0           |       h5833ebf_0          57 KB  conda-forge
    pthread-stubs-0.4          |    hd74edd7_1002           8 KB  conda-forge
    pycairo-1.27.0             |  py312h798cee4_0         103 KB  conda-forge
    pygobject-3.50.0           |  py312hc4f7465_1         316 KB  conda-forge
    pyparsing-3.2.0            |     pyhd8ed1ab_1          90 KB  conda-forge
    pyqt-5.15.9                |  py312h550cae4_5         3.8 MB  conda-forge
    pyqt5-sip-12.12.2          |  py312h9f69965_5          74 KB  conda-forge
    pyqtgraph-0.13.7           |     pyhd8ed1ab_0         1.4 MB  conda-forge
    python-dateutil-2.9.0.post0|     pyhff2d567_0         217 KB  conda-forge
    pyyaml-6.0.2               |  py312h024a12e_1         183 KB  conda-forge
    pyzmq-26.2.0               |  py312hf8a1cbd_3         353 KB  conda-forge
    qdarkstyle-3.2.3           |     pyhd8ed1ab_0         614 KB  conda-forge
    qhull-2020.2               |       h420ef59_5         504 KB  conda-forge
    qt-main-5.15.15            |       h7d33341_0        47.7 MB  conda-forge
    qtpy-2.4.2                 |     pyhdecd6ff_0          61 KB  conda-forge
    qwt-6.3.0                  |       h4ff56cd_0         3.2 MB  conda-forge
    referencing-0.35.1         |     pyhd8ed1ab_0          41 KB  conda-forge
    rpds-py-0.21.0             |  py312hcd83bfe_0         289 KB  conda-forge
    scipy-1.14.1               |  py312h20deb59_1        14.4 MB  conda-forge
    sdl-1.2.68                 |       hfc12253_0         152 KB  conda-forge
    sdl2-2.30.7                |       hf9b8971_0         1.2 MB  conda-forge
    setuptools-75.6.0          |     pyhff2d567_0         756 KB  conda-forge
    sip-6.8.6                  |  py312hde4cb15_1         607 KB  conda-forge
    six-1.16.0                 |     pyh6c4a22f_0          14 KB  conda-forge
    soapysdr-0.8.1             |  py312h6142ec9_5         553 KB  conda-forge
    spdlog-1.14.1              |       h6d8af72_1         160 KB  conda-forge
    toml-0.10.2                |     pyhd8ed1ab_0          18 KB  conda-forge
    tomli-2.1.0                |     pyhff2d567_0          18 KB  conda-forge
    uhd-4.7.0.0                |  py312h11b8700_2         5.0 MB  conda-forge
    unicodedata2-15.1.0        |  py312h0bf5046_1         364 KB  conda-forge
    volk-3.1.2                 |       hebf3989_0         328 KB  conda-forge
    wheel-0.45.1               |     pyhd8ed1ab_0          62 KB  conda-forge
    xorg-libxau-1.0.11         |       hd74edd7_1          13 KB  conda-forge
    xorg-libxdmcp-1.1.5        |       hd74edd7_0          18 KB  conda-forge
    yaml-0.2.5                 |       h3422bc3_2          86 KB  conda-forge
    zeromq-4.3.5               |       hc1bb282_7         275 KB  conda-forge
    zipp-3.21.0                |     pyhd8ed1ab_0          21 KB  conda-forge
    zlib-1.3.1                 |       h8359307_2          76 KB  conda-forge
    ------------------------------------------------------------
                                           Total:       278.4 MB

The following NEW packages will be INSTALLED:

  adwaita-icon-theme conda-forge/noarch::adwaita-icon-theme-47.0-unix_0 
  atk-1.0            conda-forge/osx-arm64::atk-1.0-2.38.0-hd03087b_2 
  attrs              conda-forge/noarch::attrs-24.2.0-pyh71513ae_0 
  brotli             conda-forge/osx-arm64::brotli-1.1.0-hd74edd7_2 
  brotli-bin         conda-forge/osx-arm64::brotli-bin-1.1.0-hd74edd7_2 
  brotli-python      conda-forge/osx-arm64::brotli-python-1.1.0-py312hde4cb15_2 
  bzip2              conda-forge/osx-arm64::bzip2-1.0.8-h99b78c6_7 
  ca-certificates    conda-forge/osx-arm64::ca-certificates-2024.8.30-hf0a4a13_0 
  cairo              conda-forge/osx-arm64::cairo-1.18.0-hb4a6bf7_3 
  certifi            conda-forge/noarch::certifi-2024.8.30-pyhd8ed1ab_0 
  cffi               conda-forge/osx-arm64::cffi-1.17.1-py312h0fad829_0 
  charset-normalizer conda-forge/noarch::charset-normalizer-3.4.0-pyhd8ed1ab_0 
  click              conda-forge/noarch::click-8.1.7-unix_pyh707e725_0 
  click-plugins      conda-forge/noarch::click-plugins-1.1.1-py_0 
  contourpy          conda-forge/osx-arm64::contourpy-1.3.1-py312hb23fbb9_0 
  cycler             conda-forge/noarch::cycler-0.12.1-pyhd8ed1ab_0 
  epoxy              conda-forge/osx-arm64::epoxy-1.5.10-h1c322ee_1 
  fftw               conda-forge/osx-arm64::fftw-3.3.10-nompi_h6637ab6_110 
  fmt                conda-forge/osx-arm64::fmt-11.0.2-h420ef59_0 
  font-ttf-dejavu-s~ conda-forge/noarch::font-ttf-dejavu-sans-mono-2.37-hab24e00_0 
  font-ttf-inconsol~ conda-forge/noarch::font-ttf-inconsolata-3.000-h77eed37_0 
  font-ttf-source-c~ conda-forge/noarch::font-ttf-source-code-pro-2.038-h77eed37_0 
  font-ttf-ubuntu    conda-forge/noarch::font-ttf-ubuntu-0.83-h77eed37_3 
  fontconfig         conda-forge/osx-arm64::fontconfig-2.15.0-h1383a14_1 
  fonts-conda-ecosy~ conda-forge/noarch::fonts-conda-ecosystem-1-0 
  fonts-conda-forge  conda-forge/noarch::fonts-conda-forge-1-0 
  fonttools          conda-forge/osx-arm64::fonttools-4.55.0-py312h998013c_0 
  freetype           conda-forge/osx-arm64::freetype-2.12.1-hadb7bae_2 
  fribidi            conda-forge/osx-arm64::fribidi-1.0.10-h27ca646_0 
  gdk-pixbuf         conda-forge/osx-arm64::gdk-pixbuf-2.42.12-h7ddc832_0 
  gettext            conda-forge/osx-arm64::gettext-0.22.5-h8414b35_3 
  gettext-tools      conda-forge/osx-arm64::gettext-tools-0.22.5-h8414b35_3 
  glib               conda-forge/osx-arm64::glib-2.82.2-hb1db9eb_0 
  glib-tools         conda-forge/osx-arm64::glib-tools-2.82.2-h25d4a46_0 
  gmp                conda-forge/osx-arm64::gmp-6.3.0-h7bae524_2 
  gnuradio           conda-forge/osx-arm64::gnuradio-3.10.11.0-py312h19d1795_5 
  gnuradio-core      conda-forge/osx-arm64::gnuradio-core-3.10.11.0-py312h9c444a1_5 
  gnuradio-grc       conda-forge/osx-arm64::gnuradio-grc-3.10.11.0-py312h14fb442_5 
  gnuradio-iio       conda-forge/osx-arm64::gnuradio-iio-3.10.11.0-py312hc9ed232_5 
  gnuradio-pmt       conda-forge/osx-arm64::gnuradio-pmt-3.10.11.0-py312h2d65830_5 
  gnuradio-qtgui     conda-forge/osx-arm64::gnuradio-qtgui-3.10.11.0-py312hd34dc92_5 
  gnuradio-soapy     conda-forge/osx-arm64::gnuradio-soapy-3.10.11.0-py312h3cf95d6_5 
  gnuradio-uhd       conda-forge/osx-arm64::gnuradio-uhd-3.10.11.0-py312hbfa0caa_5 
  gnuradio-video-sdl conda-forge/osx-arm64::gnuradio-video-sdl-3.10.11.0-py312h14fb442_5 
  gnuradio-zeromq    conda-forge/osx-arm64::gnuradio-zeromq-3.10.11.0-py312h17788b2_5 
  graphite2          conda-forge/osx-arm64::graphite2-1.3.13-hebf3989_1003 
  gsl                conda-forge/osx-arm64::gsl-2.7-h6e638da_0 
  gst-plugins-base   conda-forge/osx-arm64::gst-plugins-base-1.24.7-hb49d354_0 
  gstreamer          conda-forge/osx-arm64::gstreamer-1.24.7-hc3f5269_0 
  gstreamer-orc      conda-forge/osx-arm64::gstreamer-orc-0.4.40-hd74edd7_0 
  gtk3               conda-forge/osx-arm64::gtk3-3.24.43-h7492e44_1 
  h2                 conda-forge/noarch::h2-4.1.0-pyhd8ed1ab_0 
  harfbuzz           conda-forge/osx-arm64::harfbuzz-9.0.0-h997cde5_1 
  hicolor-icon-theme conda-forge/osx-arm64::hicolor-icon-theme-0.17-hce30654_2 
  hpack              conda-forge/noarch::hpack-4.0.0-pyh9f0ad1d_0 
  hyperframe         conda-forge/noarch::hyperframe-6.0.1-pyhd8ed1ab_0 
  icu                conda-forge/osx-arm64::icu-75.1-hfee45f7_0 
  idna               conda-forge/noarch::idna-3.10-pyhd8ed1ab_0 
  importlib-metadata conda-forge/noarch::importlib-metadata-8.5.0-pyha770c72_0 
  importlib_resourc~ conda-forge/noarch::importlib_resources-6.4.5-pyhd8ed1ab_0 
  jsonschema         conda-forge/noarch::jsonschema-4.23.0-pyhd8ed1ab_0 
  jsonschema-specif~ conda-forge/noarch::jsonschema-specifications-2024.10.1-pyhd8ed1ab_0 
  kiwisolver         conda-forge/osx-arm64::kiwisolver-1.4.7-py312h6142ec9_0 
  krb5               conda-forge/osx-arm64::krb5-1.21.3-h237132a_0 
  lame               conda-forge/osx-arm64::lame-3.100-h1a8c8d9_1003 
  lcms2              conda-forge/osx-arm64::lcms2-2.16-ha0e7c42_0 
  lerc               conda-forge/osx-arm64::lerc-4.0.0-h9a09cb3_0 
  libad9361-iio-c    conda-forge/osx-arm64::libad9361-iio-c-0.3-ha39782d_0 
  libasprintf        conda-forge/osx-arm64::libasprintf-0.22.5-h8414b35_3 
  libasprintf-devel  conda-forge/osx-arm64::libasprintf-devel-0.22.5-h8414b35_3 
  libblas            conda-forge/osx-arm64::libblas-3.9.0-25_osxarm64_openblas 
  libboost           conda-forge/osx-arm64::libboost-1.86.0-h29978a0_2 
  libbrotlicommon    conda-forge/osx-arm64::libbrotlicommon-1.1.0-hd74edd7_2 
  libbrotlidec       conda-forge/osx-arm64::libbrotlidec-1.1.0-hd74edd7_2 
  libbrotlienc       conda-forge/osx-arm64::libbrotlienc-1.1.0-hd74edd7_2 
  libcblas           conda-forge/osx-arm64::libcblas-3.9.0-25_osxarm64_openblas 
  libclang-cpp17     conda-forge/osx-arm64::libclang-cpp17-17.0.6-default_h146c034_7 
  libclang13         conda-forge/osx-arm64::libclang13-19.1.4-default_h81d93ff_0 
  libcodec2          conda-forge/osx-arm64::libcodec2-1.2.0-hd74edd7_2 
  libcxx             conda-forge/osx-arm64::libcxx-19.1.4-ha82da77_0 
  libdeflate         conda-forge/osx-arm64::libdeflate-1.22-hd74edd7_0 
  libedit            conda-forge/osx-arm64::libedit-3.1.20191231-hc8eb9b7_2 
  libevent           conda-forge/osx-arm64::libevent-2.1.12-h2757513_1 
  libexpat           conda-forge/osx-arm64::libexpat-2.6.4-h286801f_0 
  libffi             conda-forge/osx-arm64::libffi-3.4.2-h3422bc3_5 
  libflac            conda-forge/osx-arm64::libflac-1.4.3-hb765f3a_0 
  libgettextpo       conda-forge/osx-arm64::libgettextpo-0.22.5-h8414b35_3 
  libgettextpo-devel conda-forge/osx-arm64::libgettextpo-devel-0.22.5-h8414b35_3 
  libgfortran        conda-forge/osx-arm64::libgfortran-5.0.0-13_2_0_hd922786_3 
  libgfortran5       conda-forge/osx-arm64::libgfortran5-13.2.0-hf226fd6_3 
  libgirepository    conda-forge/osx-arm64::libgirepository-1.82.0-h607895c_0 
  libglib            conda-forge/osx-arm64::libglib-2.82.2-h07bd6cf_0 
  libiconv           conda-forge/osx-arm64::libiconv-1.17-h0d3ecfb_2 
  libiio-c           conda-forge/osx-arm64::libiio-c-0.26-h34e41b8_0 
  libintl            conda-forge/osx-arm64::libintl-0.22.5-h8414b35_3 
  libintl-devel      conda-forge/osx-arm64::libintl-devel-0.22.5-h8414b35_3 
  libjpeg-turbo      conda-forge/osx-arm64::libjpeg-turbo-3.0.0-hb547adb_1 
  liblapack          conda-forge/osx-arm64::liblapack-3.9.0-25_osxarm64_openblas 
  libllvm17          conda-forge/osx-arm64::libllvm17-17.0.6-h5090b49_2 
  libllvm19          conda-forge/osx-arm64::libllvm19-19.1.4-hc4b4ae8_0 
  libogg             conda-forge/osx-arm64::libogg-1.3.5-h99b78c6_0 
  libopenblas        conda-forge/osx-arm64::libopenblas-0.3.28-openmp_hf332438_1 
  libopus            conda-forge/osx-arm64::libopus-1.3.1-h27ca646_1 
  libpng             conda-forge/osx-arm64::libpng-1.6.44-hc14010f_0 
  libpq              conda-forge/osx-arm64::libpq-16.6-h0224a88_0 
  librsvg            conda-forge/osx-arm64::librsvg-2.58.4-h40956f1_0 
  libsndfile         conda-forge/osx-arm64::libsndfile-1.2.2-h9739721_1 
  libsodium          conda-forge/osx-arm64::libsodium-1.0.20-h99b78c6_0 
  libsqlite          conda-forge/osx-arm64::libsqlite-3.47.0-hbaaea75_1 
  libthrift          conda-forge/osx-arm64::libthrift-0.21.0-h64651cc_0 
  libtiff            conda-forge/osx-arm64::libtiff-4.7.0-hfce79cd_1 
  libusb             conda-forge/osx-arm64::libusb-1.0.27-h93a5062_100 
  libvorbis          conda-forge/osx-arm64::libvorbis-1.3.7-h9f76cd9_0 
  libwebp-base       conda-forge/osx-arm64::libwebp-base-1.4.0-h93a5062_0 
  libxcb             conda-forge/osx-arm64::libxcb-1.17.0-hdb1d25a_0 
  libxml2            conda-forge/osx-arm64::libxml2-2.13.5-hbbdcc80_0 
  libxslt            conda-forge/osx-arm64::libxslt-1.1.39-h223e5b9_0 
  libzlib            conda-forge/osx-arm64::libzlib-1.3.1-h8359307_2 
  llvm-openmp        conda-forge/osx-arm64::llvm-openmp-19.1.4-hdb05f8b_0 
  lxml               conda-forge/osx-arm64::lxml-5.3.0-py312ha59c1f6_2 
  mako               conda-forge/noarch::mako-1.3.6-pyhff2d567_0 
  markupsafe         conda-forge/osx-arm64::markupsafe-3.0.2-py312ha0ccf2a_0 
  matplotlib-base    conda-forge/osx-arm64::matplotlib-base-3.9.2-py312h9bd0bc6_2 
  mpg123             conda-forge/osx-arm64::mpg123-1.32.9-hf642e45_0 
  munkres            conda-forge/noarch::munkres-1.1.4-pyh9f0ad1d_0 
  mysql-common       conda-forge/osx-arm64::mysql-common-9.0.1-h0887d5e_2 
  mysql-libs         conda-forge/osx-arm64::mysql-libs-9.0.1-he9bc4e1_2 
  ncurses            conda-forge/osx-arm64::ncurses-6.5-h7bae524_1 
  nspr               conda-forge/osx-arm64::nspr-4.36-h5833ebf_0 
  nss                conda-forge/osx-arm64::nss-3.107-hc555b47_0 
  numpy              conda-forge/osx-arm64::numpy-2.1.3-py312h94ee1e1_0 
  openjpeg           conda-forge/osx-arm64::openjpeg-2.5.2-h9f1df11_0 
  openssl            conda-forge/osx-arm64::openssl-3.4.0-h39f12f2_0 
  packaging          conda-forge/noarch::packaging-24.2-pyhff2d567_1 
  pango              conda-forge/osx-arm64::pango-1.54.0-h9ee27a3_2 
  pcre2              conda-forge/osx-arm64::pcre2-10.44-h297a79d_2 
  pillow             conda-forge/osx-arm64::pillow-11.0.0-py312haf37ca6_0 
  pip                conda-forge/noarch::pip-24.3.1-pyh8b19718_0 
  pixman             conda-forge/osx-arm64::pixman-0.43.4-hebf3989_0 
  pkgutil-resolve-n~ conda-forge/noarch::pkgutil-resolve-name-1.3.10-pyhd8ed1ab_1 
  ply                conda-forge/noarch::ply-3.11-pyhd8ed1ab_2 
  portaudio          conda-forge/osx-arm64::portaudio-19.7.0-h5833ebf_0 
  pthread-stubs      conda-forge/osx-arm64::pthread-stubs-0.4-hd74edd7_1002 
  pybind11-abi       conda-forge/noarch::pybind11-abi-4-hd8ed1ab_3 
  pycairo            conda-forge/osx-arm64::pycairo-1.27.0-py312h798cee4_0 
  pycparser          conda-forge/noarch::pycparser-2.22-pyhd8ed1ab_0 
  pygobject          conda-forge/osx-arm64::pygobject-3.50.0-py312hc4f7465_1 
  pyparsing          conda-forge/noarch::pyparsing-3.2.0-pyhd8ed1ab_1 
  pyqt               conda-forge/osx-arm64::pyqt-5.15.9-py312h550cae4_5 
  pyqt5-sip          conda-forge/osx-arm64::pyqt5-sip-12.12.2-py312h9f69965_5 
  pyqtgraph          conda-forge/noarch::pyqtgraph-0.13.7-pyhd8ed1ab_0 
  pysocks            conda-forge/noarch::pysocks-1.7.1-pyha2e5f31_6 
  python             conda-forge/osx-arm64::python-3.12.7-h739c21a_0_cpython 
  python-dateutil    conda-forge/noarch::python-dateutil-2.9.0.post0-pyhff2d567_0 
  python_abi         conda-forge/osx-arm64::python_abi-3.12-5_cp312 
  pyyaml             conda-forge/osx-arm64::pyyaml-6.0.2-py312h024a12e_1 
  pyzmq              conda-forge/osx-arm64::pyzmq-26.2.0-py312hf8a1cbd_3 
  qdarkstyle         conda-forge/noarch::qdarkstyle-3.2.3-pyhd8ed1ab_0 
  qhull              conda-forge/osx-arm64::qhull-2020.2-h420ef59_5 
  qt-main            conda-forge/osx-arm64::qt-main-5.15.15-h7d33341_0 
  qtpy               conda-forge/noarch::qtpy-2.4.2-pyhdecd6ff_0 
  qwt                conda-forge/osx-arm64::qwt-6.3.0-h4ff56cd_0 
  readline           conda-forge/osx-arm64::readline-8.2-h92ec313_1 
  referencing        conda-forge/noarch::referencing-0.35.1-pyhd8ed1ab_0 
  requests           conda-forge/noarch::requests-2.32.3-pyhd8ed1ab_0 
  rpds-py            conda-forge/osx-arm64::rpds-py-0.21.0-py312hcd83bfe_0 
  ruamel.yaml        conda-forge/osx-arm64::ruamel.yaml-0.18.6-py312h0bf5046_1 
  ruamel.yaml.clib   conda-forge/osx-arm64::ruamel.yaml.clib-0.2.8-py312h0bf5046_1 
  scipy              conda-forge/osx-arm64::scipy-1.14.1-py312h20deb59_1 
  sdl                conda-forge/osx-arm64::sdl-1.2.68-hfc12253_0 
  sdl2               conda-forge/osx-arm64::sdl2-2.30.7-hf9b8971_0 
  setuptools         conda-forge/noarch::setuptools-75.6.0-pyhff2d567_0 
  sip                conda-forge/osx-arm64::sip-6.8.6-py312hde4cb15_1 
  six                conda-forge/noarch::six-1.16.0-pyh6c4a22f_0 
  soapysdr           conda-forge/osx-arm64::soapysdr-0.8.1-py312h6142ec9_5 
  spdlog             conda-forge/osx-arm64::spdlog-1.14.1-h6d8af72_1 
  tk                 conda-forge/osx-arm64::tk-8.6.13-h5083fa2_1 
  toml               conda-forge/noarch::toml-0.10.2-pyhd8ed1ab_0 
  tomli              conda-forge/noarch::tomli-2.1.0-pyhff2d567_0 
  tzdata             conda-forge/noarch::tzdata-2024b-hc8b5060_0 
  uhd                conda-forge/osx-arm64::uhd-4.7.0.0-py312h11b8700_2 
  unicodedata2       conda-forge/osx-arm64::unicodedata2-15.1.0-py312h0bf5046_1 
  urllib3            conda-forge/noarch::urllib3-2.2.3-pyhd8ed1ab_0 
  volk               conda-forge/osx-arm64::volk-3.1.2-hebf3989_0 
  wheel              conda-forge/noarch::wheel-0.45.1-pyhd8ed1ab_0 
  xorg-libxau        conda-forge/osx-arm64::xorg-libxau-1.0.11-hd74edd7_1 
  xorg-libxdmcp      conda-forge/osx-arm64::xorg-libxdmcp-1.1.5-hd74edd7_0 
  xz                 conda-forge/osx-arm64::xz-5.2.6-h57fd34a_0 
  yaml               conda-forge/osx-arm64::yaml-0.2.5-h3422bc3_2 
  zeromq             conda-forge/osx-arm64::zeromq-4.3.5-hc1bb282_7 
  zipp               conda-forge/noarch::zipp-3.21.0-pyhd8ed1ab_0 
  zlib               conda-forge/osx-arm64::zlib-1.3.1-h8359307_2 
  zstandard          conda-forge/osx-arm64::zstandard-0.23.0-py312h15fbf35_1 
  zstd               conda-forge/osx-arm64::zstd-1.5.6-hb46c0d2_0 


Proceed ([y]/n)? y


Downloading and Extracting Packages:
                                                                                                                                          
Preparing transaction: done                                                                                                               
Verifying transaction: done                                                                                                               
Executing transaction: -                                                                                                                  
/                                                                                                                                         
|                                                                                                                                         
done                                                                                                                                      
#                                                                                                                                         
# To activate this environment, use                                                                                                       
#                                                                                                                                         
#     $ conda activate gnuradio-env                                                                                                       
#                                                                                                                                         
# To deactivate an active environment, use                                                                                                
#                                                                                                                                         
#     $ conda deactivate                 
```

Thank you for installing Miniforge3!
```
