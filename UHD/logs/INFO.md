```
apt info gnuradio
```
> Returns
```
Package: gnuradio
Version: 3.10.7.0-3build1
Priority: optional
Section: universe/comm
Origin: Ubuntu
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: A. Maitland Bottoms <bottoms@debian.org>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 22.8 MB
Depends: libjs-mathjax, libvolk-bin, python3-click, python3-click-plugins, python3-gi, python3-gi-cairo, python3-jsonschema, python3-lxml, python3-mako, python3-numpy (>= 1:1.22.0), python3-opengl, python3-packaging, python3-pygccxml, python3-pyqt5, python3-pyqtgraph, python3-schema, python3-sip, python3-thrift, python3-yaml, python3-zmq, gnome-terminal | x-terminal-emulator, python3 (<< 3.12), python3 (>= 3.11~), python3-numpy-abi9, python3:any, libboost-program-options1.74.0 (>= 1.74.0+ds1), libc6 (>= 2.38), libfmt9 (>= 9.1.0+ds1), libgcc-s1 (>= 4.5), libgmp10 (>= 2:6.3.0+dfsg), libgnuradio-analog3.10.7 (>= 3.10.7.0), libgnuradio-audio3.10.7 (>= 3.10.7.0), libgnuradio-blocks3.10.7 (>= 3.10.7.0), libgnuradio-channels3.10.7 (>= 3.10.7.0), libgnuradio-digital3.10.7 (>= 3.10.7.0), libgnuradio-dtv3.10.7 (>= 3.10.7.0), libgnuradio-fec3.10.7 (>= 3.10.7.0), libgnuradio-fft3.10.7 (>= 3.10.7.0), libgnuradio-filter3.10.7 (>= 3.10.7.0), libgnuradio-iio3.10.7 (>= 3.10.7.0), libgnuradio-network3.10.7 (>= 3.10.7.0), libgnuradio-pdu3.10.7 (>= 3.10.7.0), libgnuradio-pmt3.10.7 (>= 3.10.7.0), libgnuradio-qtgui3.10.7 (>= 3.10.7.0), libgnuradio-runtime3.10.7 (>= 3.10.7.0), libgnuradio-soapy3.10.7 (>= 3.10.7.0), libgnuradio-trellis3.10.7 (>= 3.10.7.0), libgnuradio-uhd3.10.7 (>= 3.10.7.0), libgnuradio-video-sdl3.10.7 (>= 3.10.7.0), libgnuradio-vocoder3.10.7 (>= 3.10.7.0), libgnuradio-wavelet3.10.7 (>= 3.10.7.0), libgnuradio-zeromq3.10.7 (>= 3.10.7.0), libqt5core5a (>= 5.0.2), libqt5widgets5 (>= 5.0.2), libsoapysdr0.8 (>= 0.8.1), libspdlog1.10-fmt9, libstdc++6 (>= 13.1), libuhd4.4.0 (>= 4.4.0.0+ds1), libvolk3.0 (>= 3.0.0)
Recommends: gnuradio-dev, python3-pygccxml (>= 2.0), python3-matplotlib, python3-networkx, python3-pyqt5.qwt, python3-scipy, soapysdr-tools
Suggests: gr-fosphor, gr-osmosdr, gqrx-sdr, rtl-sdr, uhd-host
Breaks: gr-iio, gr-soapy
Replaces: gr-iio, gr-soapy
Homepage: https://www.gnuradio.org/
Download-Size: 3537 kB
APT-Manual-Installed: no
APT-Sources: http://ports.ubuntu.com/ubuntu-ports mantic/universe arm64 Packages
Description: GNU Radio Software Radio Toolkit
 GNU Radio provides signal processing blocks to implement software
 radios. It can be used with readily-available low-cost external RF
 hardware to create software-defined radios, or without hardware in a
 simulation-like environment. It is widely used in hobbyist, academic
 and commercial environments to support both wireless communications
 research and real-world radio systems.
 .
 GNU Radio applications are primarily written using the Python
 programming language, while the supplied performance-critical signal
 processing path is implemented in C++ using processor floating-point
 extensions, where available. Thus, the developer is able to implement
 real-time, high-throughput radio systems in a simple-to-use,
 rapid-application-development environment.
 .
 While not primarily a simulation tool, GNU Radio does support
 development of signal processing algorithms using pre-recorded or
 generated data, avoiding the need for actual RF hardware.
 .
 This package contains the gnuradio-companion, a graphical tool for
 creating signal flow graphs and generating flow-graph source code.
 Also included are a variety of tools and utility programs.


```
