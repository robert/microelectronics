# UHD


### Install on &#x1F427; Linux (Debian)

```
sudo apt-get install libuhd-dev uhd-host
```

* Install Images

```
sudo mkdir -p /usr/share/uhd/images
```

```
sudo chown -R $(whoami):$(id -gn) /usr/share/uhd/images
```

```
uhd_images_downloader
```
> Returns
```
[INFO] Using base URL: https://files.ettus.com/binaries/cache/
[INFO] Images destination: /usr/share/uhd/images
[INFO] No inventory file found at /usr/share/uhd/images/inventory.json. Creating an empty one.
21099 kB / 21099 kB (100%) x4xx_x410_fpga_default-g92c09f7.zip
21651 kB / 21651 kB (100%) x3xx_x310_fpga_default-g92c09f7.zip
20045 kB / 20045 kB (100%) x3xx_x300_fpga_default-g92c09f7.zip
01125 kB / 01125 kB (100%) e3xx_e310_sg1_fpga_default-g92c09f7.zip
01119 kB / 01119 kB (100%) e3xx_e310_sg3_fpga_default-g92c09f7.zip
10184 kB / 10184 kB (100%) e3xx_e320_fpga_default-g92c09f7.zip
20915 kB / 20915 kB (100%) n3xx_n310_fpga_default-g92c09f7.zip
14281 kB / 14281 kB (100%) n3xx_n300_fpga_default-g92c09f7.zip
27224 kB / 27224 kB (100%) n3xx_n320_fpga_default-g92c09f7.zip
00481 kB / 00481 kB (100%) b2xx_b200_fpga_default-g92c09f7.zip
00464 kB / 00464 kB (100%) b2xx_b200mini_fpga_default-g92c09f7.zip
00883 kB / 00883 kB (100%) b2xx_b210_fpga_default-g92c09f7.zip
00511 kB / 00511 kB (100%) b2xx_b205mini_fpga_default-g92c09f7.zip
00167 kB / 00167 kB (100%) b2xx_common_fw_default-g7f7d016.zip
00007 kB / 00007 kB (100%) usrp2_usrp2_fw_default-g6bea23d.zip
00450 kB / 00450 kB (100%) usrp2_usrp2_fpga_default-g6bea23d.zip
02415 kB / 02415 kB (100%) usrp2_n200_fpga_default-g6bea23d.zip
00009 kB / 00009 kB (100%) usrp2_n200_fw_default-g6bea23d.zip
02757 kB / 02757 kB (100%) usrp2_n210_fpga_default-g6bea23d.zip
00009 kB / 00009 kB (100%) usrp2_n210_fw_default-g6bea23d.zip
00319 kB / 00319 kB (100%) usrp1_usrp1_fpga_default-g6bea23d.zip
00522 kB / 00522 kB (100%) usrp1_b100_fpga_default-g6bea23d.zip
00006 kB / 00006 kB (100%) usrp1_b100_fw_default-g6bea23d.zip
00017 kB / 00017 kB (100%) octoclock_octoclock_fw_default-g14000041.zip
04839 kB / 04839 kB (100%) usb_common_windrv_default-g14000041.zip
[INFO] Images download complete.
```

- [ ] Find devices

```
uhd_find_devices
```
> Returns
```
[INFO] [UHD] linux; GNU C++ version 13.1.0; Boost_107400; UHD_4.4.0.0+ds1-4
No UHD Devices Found
```

```
uhd_find_devices
```
> Returns
```
[INFO] [UHD] linux; GNU C++ version 13.1.0; Boost_107400; UHD_4.4.0.0+ds1-4
[INFO] [B200] Loading firmware image: /usr/share/uhd/images/usrp_b200_fw.hex...
--------------------------------------------------
-- UHD Device 0
--------------------------------------------------
Device Address:
    serial: 30C51F5
    name: B200mini
    product: B200mini
    type: b200
```

##### USB Rules

[Setup Udev for USB (Linux)](https://files.ettus.com/manual/page_transport.html#:~:text=udevadm,rules)

On Linux, Udev handles USB plug and unplug events. The following commands install a Udev rule so that non-root users may access the device:

`<install-path>` = /usr/lib      # on Raspbian  

`<install-path>` = /usr/libexec  # on Ubuntu

```
cd <install-path>/uhd/utils
sudo cp uhd-usrp.rules /etc/udev/rules.d/
sudo udevadm control --reload-rules
sudo udevadm trigger
```

* The USB Error associated with above the missing configuration

```powershell
[INFO] [UHD] linux; GNU C++ version 13.1.0; Boost_107400; UHD_4.4.0.0+ds1-4
[ERROR] [USB] USB open failed: insufficient permissions.
See the application notes for your device.

No UHD Devices Found
```

### MacOS

- [ ] Install [MacPorts](https://www.macports.org) for :apple:

- Download Ports binary [package](https://github.com/macports/macports-base/releases/download/v2.8.1/MacPorts-2.8.1-14-Sonoma.pkg)
- Installl the package
- Update the `~/.zshrc`  to include PATH linked to `/opt/local/bin` [(1)](https://superuser.com/questions/287713/sudo-port-command-not-found-after-installing-macports-on-snow-leopard)

- [ ] Install UHD on :apple: using [MacPorts](https://ports.macports.org/port/uhd/)
  
  ```
  sudo port install uhd
  ```
  > Returns:
```powershell
--->  Fetching archive for uhd
--->  Attempting to fetch uhd-3.15.0.0_7+docs+examples+libusb+manpages+manual+python310+python_api+test.darwin_23.arm64.tbz2 from https://packages.macports.org/uhd
--->  Attempting to fetch uhd-3.15.0.0_7+docs+examples+libusb+manpages+manual+python310+python_api+test.darwin_23.arm64.tbz2 from https://fra.de.packages.macports.org/uhd
--->  Attempting to fetch uhd-3.15.0.0_7+docs+examples+libusb+manpages+manual+python310+python_api+test.darwin_23.arm64.tbz2 from https://mse.uk.packages.macports.org/uhd
--->  Fetching distfiles for uhd
--->  Attempting to fetch uhd-3.15.0.0.tar.gz from https://distfiles.macports.org/uhd
--->  Verifying checksums for uhd                                                
--->  Extracting uhd
--->  Applying patches to uhd
--->  Configuring uhd
--->  Building uhd                                       
--->  Staging uhd into destroot                          
--->  Installing uhd @3.15.0.0_7+docs+examples+libusb+manpages+manual+python310+python_api+test
--->  Activating uhd @3.15.0.0_7+docs+examples+libusb+manpages+manual+python310+python_api+test
--->  Cleaning uhd
--->  Updating database of binaries
--->  Scanning binaries for linking errors
--->  No broken files found.
--->  No broken ports found.
--->  Some of the ports you installed have notes:
  db48 has the following notes:
    The Java and Tcl bindings are now provided by the db48-java and
    db48-tcl subports.
  libpsl has the following notes:
    libpsl API documentation is provided by the port 'libpsl-docs'.
  lzma has the following notes:
    The LZMA SDK program is installed as "lzma_alone", to avoid conflict with
    LZMA Utils
  py310-cython has the following notes:
    To make the Python 3.10 version of Cython the one that is run when you
    execute the commands without a version suffix, e.g. 'cython', run:
    
    port select --set cython cython310
  py310-docutils has the following notes:
    To make the Python 3.10 version of docutils the one that is run when you
    execute the commands without a version suffix, e.g. 'rst2man', run:
    
    port select --set docutils py310-docutils
  python310 has the following notes:
    To make this the default Python or Python 3 (i.e., the version run by the
    'python' or 'python3' commands), run one or both of:
    
        sudo port select --set python python310
        sudo port select --set python3 python310
  python311 has the following notes:
    To make this the default Python or Python 3 (i.e., the version run by the
    'python' or 'python3' commands), run one or both of:
    
        sudo port select --set python python311
        sudo port select --set python3 python311
```

  ```
  sudo port info uhd   
  ```
  > Returns:
```powershell
Password:
uhd @3.15.0.0_7 (science, comms)
Sub-ports:            uhd-39lts, uhd-devel
Variants:             debug, [+]docs, [+]examples, [+]libusb, [+]manpages, [+]manual, python27, [+]python310,
                      python35, python36, python37, python38, python39, [+]python_api, [+]test, universal

Description:          USRP Hardware Driver for Ettus Research Products: Provides the release version, which is
                      typically updated every month or so.
Homepage:             https://kb.ettus.com/UHD

Build Dependencies:   cmake, pkgconfig, doxygen
Library Dependencies: ncurses, python310, py310-mako, py310-requests, py310-six, libusb, py310-docutils, gzip,
                      py310-numpy, py310-pybind11, boost171
Conflicts with:       uhd-devel, uhd-39lts
Platforms:            darwin
License:              GPL-3+
Maintainers:          Email: michaelld@macports.org, GitHub: michaelld
```

#### Windows 11

- [ ] [Windows-10-x64/uhd_4.6.0.0-release_Win64_VS2019.exe](https://files.ettus.com/binaries/uhd/latest_release/4.6.0.0/Windows-10-x64/uhd_4.6.0.0-release_Win64_VS2019.exe)
- [ ] [WSL Connect USB devices](https://learn.microsoft.com/en-us/windows/wsl/connect-usb)


# References

#### :o: [Installation](https://files.ettus.com/manual/page_install.html)

- [ ] [The Core Values of the Xilinx Spartan 6 XC6SLX75 FPGA](https://ebics.net/xilinx-spartan-6-xc6slx75/)
- [ ] [When trying to use my USRP in GNU Radio, I get a " No devices found for ----->" error](https://stackoverflow.com/questions/33304828/when-trying-to-use-my-usrp-in-gnu-radio-i-get-a-no-devices-found-for)
- [ ] [Permission issue with USB](https://forums.raspberrypi.com/viewtopic.php?t=186839)

