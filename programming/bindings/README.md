```
export UHD_HOME=/usr/share/uhd
export UHD_IMAGES_DIR=${UHD_HOME}/images
```

```
julia src/setup.jl 
```
> Returns
```bash
[INFO] [UHD] linux; GNU C++ version 7.1.0; Boost_107600; UHD_4.1.0.HEAD-0-g6bd0be9c
[INFO] [B200] Detected Device: B200mini
[INFO] [B200] Loading FPGA image: /usr/share/uhd/images/usrp_b200mini_fpga.bin...
[INFO] [B200] Operating over USB 3.
[INFO] [B200] Initialize CODEC control...
[INFO] [B200] Initialize Radio control...
[INFO] [B200] Performing register loopback test... 
[INFO] [B200] Register loopback test passed
[INFO] [B200] Setting master clock rate selection to 'automatic'.
[INFO] [B200] Asking for clock rate 16.000000 MHz... 
[INFO] [B200] Actually got clock rate 16.000000 MHz.
┌Rx Warning: Effective carrier frequency is 867.9999999999992 MHz and not 868.0 MHz
┌Tx Warning: Effective carrier frequency is 867.9999999999992 MHz and not 868.0 MHz
┌Rx: Current UHD Configuration in Rx mode
| Carrier Frequency: 868.000 MHz
| Sampling Frequency: 16.000 MHz
└  Rx Gain: 30.00 dB
┌Tx: Current UHD Configuration in Tx mode
| Carrier Frequency: 868.000 MHz
| Sampling Frequency: 16.000 MHz
└  Tx Gain: 30.00 dB

[ Info: USRP device is now closed.
```
